const express = require("express");
//Mongoose is a package that allows creation of Schemas to model our data structures
//Also has access to a number of methods for manipulation our database
const mongoose = require("mongoose");

const app = express();

const port = 3001;
//MongoDB Atlas Connection
//Connect to the database by passing in your connection string, remember to replace the password and database names with actual values.
//when we want to use local mongoDB/robo3t
//mongoose.connect("mongodb://localhost:27017/databaseName")

mongoose.connect('mongodb+srv://kjames127:kjamesvin@cluster0.ba0fr.mongodb.net/batch164_to-do?retryWrites=true&w=majority', 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
)
//Due to updates in mongodb drivers that allow connection to it, the default connection string is being flagged as an error
//By default a warning will be displayed in the terminal when the app is running, but this will not prevent mongoose from being used in the app.
//to prevent/avoid any current and future errors while connecting to Mongo DB, add the userNewUrlParser and useUnifiedTopology.

//Set notifications for connection success or failure
let db = mongoose.connection;

//console.error.binde(console) allows us to print errors in the browser console and in the terminal
//"connection error" is the message that will display if an error is encountered.
db.on("error", console.error.bind(console, "connection error"));
//connection successful message
db.once("open", () => console.log("We're connected to the cloud database"))




app.use(express.json());

app.use(express.urlencoded({ extended:true }));


//Mongoose Schemas
//Schemas determine the structure of the documents to be written in the database
//act as blueprint to our data
//Use Schema() constructor of the Mongoose module to create a new Schema object
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		//Default values are the predefined values for a field if we don't put any value.
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);

//Routes/endpoints

//Creating a new task

//Business Logic
/*
1. Add a functionality to check if there are duplicates tasks
	-If the task already exist in the database, we return error
	-If the task doesn't exist in the database, we add it in the database
		1. The task data will be coming from the request's body.
		2. Create a new Task object with field/property
		3. save the new object to our database.
*/

app.post("/tasks", (req, res) => {
	Task.findOne({ name: req.body.name }, (err, result) => {
		//If a document was found and the document's name matches the information sent via the client
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found")
		} else{
			//if no document was found
			//create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save( (saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr)
				}else {
					return res.status(201).send("New task created")
				}

			})
		}

	})
})

//Get all tasks
//Business Logic
/*
1. Find/retrieve all the documents
2. if an error is encountered, print error
3. if no errors are found, send a success status back to the client and return an array of documents

*/
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				dataFromMDB: result
			})
		}
	})
})



//=============ACTIVITY===============



const userSchema = new mongoose.Schema({
	username: String,
	password: String
})
const User = mongoose.model("User", userSchema)

app.post("/signup", (req,res)=>{
	User.findOne( {
		username:req.body.username
	}, (err, result) => {
				if (result != null && result.username == req.body.username) {
					return res.send (`This username already exists`)
				}else {
					let newUser = new User ({
							username: req.body.username,
							password: req.body.password
							})
						newUser.save((saveErr, savedTask) => {
								if (saveErr) {
									return console.error(saveErr)
								}
								else{
									return res.status(201).send (`New user created`)
								}	
							})
					
				}
			} )
} )

app.get("/users", (req, res)=>{
	User.find( {}, (err, result)=>{
		if (err	) {
		return console.error(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}

	} )

})











app.listen(port, ()=> console.log(`Server running at port ${port}`));
























